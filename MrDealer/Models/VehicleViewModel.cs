﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MrDealer.Models
{
    public class VehicleViewModel
    {
        public System.Guid Id { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public System.Guid BodyTypeId { get; set; }
        public System.Guid VehicleMakeId { get; set; }
        public System.Guid VehicleTypeId { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> DeactivatedDate { get; set; }
        public string Image { get; set; }
    }
}