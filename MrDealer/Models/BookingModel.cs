﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MrDealer.Models
{
    public class BookingModel
    {
        public Guid VehicleId { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerFullName { get; set; }
        public string CellNumber { get; set; }
        public string TestDriveVehicleModel { get; set; }

    }
}