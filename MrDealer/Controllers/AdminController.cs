﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MrDealer.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Makes()
        {
            return View();
        }
        public ActionResult VehicleTypes()
        {
            return View();
        }
        public ActionResult BodyTypes()
        {
            return View();
        }
        public ActionResult Models()
        {
            return View();
        }
    }
}