﻿using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MrDealer.Controllers.API
{
    public class BookingController : ApiController
    {
        private readonly IBookingBLLService _bll;
        private readonly IVehicleModelBLLService _vehicleBll;

        public BookingController(IBookingBLLService bll, IVehicleModelBLLService vehicleBll)
        {
            _bll = bll;
            _vehicleBll = vehicleBll;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Save(TestDriveBooking model)
        {
            try
            {
                return Ok(await _bll.Save(model));
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetBookings()
        {
            try
            {
                var entities = await _bll.GetAll();
                var results = entities.Select(item => new BookingModel { Date = item.ScheduledDate.ToShortDateString(), Time = item.Time, CellNumber = "TODO", CustomerFullName = "TODO" ,VehicleId=item.VehicleModelId }).ToList();
                foreach (var item in results)
                {
                    var model = await _vehicleBll.GetAll(c => c.Id == item.VehicleId);
                    item.TestDriveVehicleModel = $"{model.FirstOrDefault().Year} - {model.FirstOrDefault().Name}" ;
                }
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
