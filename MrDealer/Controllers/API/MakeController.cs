﻿using MrDealer.BLL.Services;
using MrDealer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MrDealer.Controllers.API
{
    public class MakeController : ApiController
    {
        private readonly IVehicleMakeBLLService _bll;
        public MakeController(IVehicleMakeBLLService bll)
        {
            _bll = bll;
        }
        [HttpPost]
        public async Task<IHttpActionResult> Save(VehicleMake make)
        {
            try
            {
                var results = await _bll.Save(make);
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var results = await _bll.GetAll();
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> Search(string term)
        {
            try
            {
                var results = string.IsNullOrEmpty(term) ? await _bll.GetAll() : await _bll.GetAll(c=>c.Name.ToLower().Contains(term.ToLower()));
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
