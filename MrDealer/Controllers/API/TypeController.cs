﻿using MrDealer.BLL.Providers;
using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MrDealer.Controllers.API
{
    public class TypeController : ApiController
    {
        private readonly IVehicleTypeBLLService _bll;
        public TypeController(IVehicleTypeBLLService bll)
        {
            _bll = bll;
        }
        [HttpPost]
        public async Task<IHttpActionResult> Save(VehicleType type)
        {
            try
            {
                var results = await _bll.Save(type);
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var results = await _bll.GetAll();
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> Search(string term)
        {
            try
            {
                var results = string.IsNullOrEmpty(term) ? await _bll.GetAll() : await _bll.GetAll(c => c.Name.ToLower().Contains(term.ToLower()));
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
