﻿using MrDealer.BLL.Providers;
using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.DAL.Providers;
using MrDealer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MrDealer.Controllers.API
{
    public class VehicleController : ApiController
    {
        private readonly IVehicleModelBLLService _bll;
        public VehicleController(IVehicleModelBLLService bll)
        {
            _bll = bll;
        }
        [HttpPost]
        public async Task<IHttpActionResult> Save(VehicleViewModel vehicle)
        {
            try
            {
                var entity = new VehicleModel
                {
                    BodyTypeId = vehicle.BodyTypeId,
                    DeactivatedDate = vehicle.DeactivatedDate,
                    Id = vehicle.Id,
                    Image = string.IsNullOrEmpty(vehicle.Image) ? GetDefaultImage(): Convert.FromBase64String(vehicle.Image.Split(',').LastOrDefault()), 
                    IsActive = vehicle.IsActive, 
                    Name = vehicle.Name, 
                    VehicleMakeId = vehicle.VehicleMakeId, 
                    VehicleTypeId = vehicle.VehicleTypeId, 
                    Year = vehicle.Year 
                };
                var results = await _bll.SaveModel(entity);
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        private byte[] GetDefaultImage() =>  new byte[225];

        [HttpGet]
        public async Task<IHttpActionResult> GetModelsByMake(Guid makeId)
        {
            try
            {
                var results = await _bll.GetAll(c => c.VehicleMakeId == makeId); ;
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var entities = await _bll.GetAll();
                var results = entities.Select(item => new VehicleViewModel { BodyTypeId = item.BodyTypeId, DeactivatedDate = item.DeactivatedDate, Id = item.Id, Image = "data:image/jpeg;base64," + Convert.ToBase64String(item.Image ?? GetDefaultImage()), IsActive = item.IsActive, Name = item.Name, VehicleMakeId = item.VehicleMakeId, VehicleTypeId = item.VehicleTypeId, Year = item.Year }).ToList();
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> Search(string term)
        {
            try
            {
                var entities = string.IsNullOrEmpty(term) ? await _bll.GetAll() : await _bll.GetAll(c => c.Name.ToLower().Contains(term.ToLower()));
                var results = entities.Select(item => new VehicleViewModel { BodyTypeId = item.BodyTypeId, DeactivatedDate = item.DeactivatedDate, Id = item.Id, Image = "data:image/jpeg;base64," + Convert.ToBase64String(item.Image ?? GetDefaultImage()), IsActive = item.IsActive, Name = item.Name, VehicleMakeId = item.VehicleMakeId, VehicleTypeId = item.VehicleTypeId, Year = item.Year }).ToList();
                return Ok(results);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
