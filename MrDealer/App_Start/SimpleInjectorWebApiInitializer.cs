[assembly: WebActivator.PostApplicationStartMethod(typeof(MrDealer.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace MrDealer.App_Start
{
    using System.Web.Http;
    using MrDealer.BLL.Providers;
    using MrDealer.BLL.Services;
    using MrDealer.Core;
    using MrDealer.DAL.Providers;
    using MrDealer.DAL.Services;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using SimpleInjector.Lifestyles;
    
    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            
            InitializeContainer(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
       
            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        private static void InitializeContainer(Container container)
        {

            // For instance:
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);

            container.Register<IVehicleMakeBLLService, VehicleMakeBLL>(Lifestyle.Scoped);
            container.Register<IVehicleTypeBLLService, VehicleTypeBLL>(Lifestyle.Scoped);
            container.Register<IBodyTypeBLLService, BodyTypeBLL>(Lifestyle.Scoped);
            container.Register<IVehicleModelBLLService, VehicleModelBLL>(Lifestyle.Scoped);
            container.Register<IBookingBLLService, BookingBLL>(Lifestyle.Scoped);

            container.Register<IVehicleMakeDALService, VehicleMakeDAL>(Lifestyle.Scoped);
            container.Register<IVehicleTypeDALService, VehicleTypeDAL>(Lifestyle.Scoped);
            container.Register<IBodyTypeDALService, BodyTypeDAL>(Lifestyle.Scoped);
            container.Register<IVehicleModelDALService, VehicleModelDAL>(Lifestyle.Scoped);
            container.Register<IBookingDALService, BookingDAL>(Lifestyle.Scoped);

            container.Register<MyDealershipEntities>(Lifestyle.Scoped);
        }
    }
}