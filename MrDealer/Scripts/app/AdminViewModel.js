﻿function AdminViewModel() {

    self = this;

    BaseViewModel.call(self);

    self.Bookings = ko.observableArray();
    self.Items = ko.observableArray();


    self.SearchInput = ko.observable("").extend({ notify: 'always' });;
    self.Id = ko.observable("").extend({ notify: 'always' });;
    self.Name = ko.observable("").extend({ notify: 'always' });;
    self.IsActive = ko.observable(false).extend({ notify: 'always' });;


    
}