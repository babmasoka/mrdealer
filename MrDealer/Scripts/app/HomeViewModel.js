﻿function HomeViewModel() {
    self = this;

    BaseViewModel.call(self);
    self.Vehicles = ko.observableArray();
    self.VehicleModelId = ko.observable("");
    self.Date = ko.observable("");
    self.Time = ko.observable("");
    self.CustomerId = ko.observable("");
    self.SearchInput = ko.observable("");
    self.ConfirmationText = ko.observable("");

    self.IsBusy(true);
    self.Get("/api/vehicle/get",
        function (data) {
            self.Vehicles(data);
            self.IsBusy(false);
        },
        function (error) {
            alert(error.response)
            self.IsBusy(false);
        });


    self.processSearch = function () {
        self.IsBusy(true);
        self.Get("/api/vehicle/search?term=" + self.SearchInput(),
            function (data) {
                self.Vehicles(data);
                self.IsBusy(false);
            },
            function (error) {
                alert(error.response)
                self.IsBusy(false);
            })
    };

    self.showModal = function (item) {   

        self.VehicleModelId(item.id);
        self.ConfirmationText('Please note you are about to make a test driving appointment for the ' + item.year + ' - ' + item.name);
        $('#modal-default').modal('show');
    };

    $(document).on("click", "#btnSubmit", function (event) {
        if (self.Date() !== "" || self.Time() !== "") {
            var objectToSave = {
                ScheduledDate: self.Date(),
                Time: self.Time(),
                VehicleModelId: self.VehicleModelId(),
                IsActive: true
            };
            self.IsBusy(true);
            self.Post("/api/booking/save",
                objectToSave,
                function (data) {
                    alert("Successful");
                    self.Date('');
                    self.Time('');
                    self.VehicleModelId('');
                    $('#modal-default').modal('hide');
                    self.Get("/api/booking/getbookings",
                        function (savedData) {
                            self.Vehicles(savedData);
                            self.IsBusy(false);
                        },
                        function (error) {
                            alert(error.response)
                            self.IsBusy(false);
                        });
                },
                function (error) {
                    alert(error);
                    self.IsBusy(false);
                }
            );
        }
        else {
            alert("Please fill in all fields.");
        }
    });

    self.processSave = function () {
        
    };
}