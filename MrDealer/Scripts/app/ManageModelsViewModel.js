﻿function ManageModelsViewModel() {

    self = this;

    BaseViewModel.call(self);

    self.Id = ko.observable("");
    self.MakeId = ko.observable("");
    self.BodyTypeId = ko.observable("");
    self.VehicleTypeId = ko.observable("");
    self.Name = ko.observable("");
    self.Year = ko.observable("");
    self.IsActive = ko.observable(false);

    self.photoUrl = ko.observable();
    self.SearchInput = ko.observable("");

    self.Makes = ko.observableArray();
    self.BodyTypes = ko.observableArray();
    self.VehicleTypes = ko.observableArray();
    self.Vehicles = ko.observableArray();
    self.Items = ko.observableArray().extend({ notify: 'always' });

    
    self.IsBusy(true);
    // Makes //
    self.Get("/api/make/get",
        function (data) {
            self.Makes(data);
            self.IsBusy(false);
        },
        function (error) {
            alert(error.response)
            self.IsBusy(false);
        });
    // BODY TYPES //
    self.Get("/api/bodytype/get",
        function (data) {
            self.BodyTypes(data);
            self.IsBusy(false);
        },
        function (error) {
            alert(error.response)
            self.IsBusy(false);
        });
    // VEHICLE TYPES //
    self.Get("/api/type/get",
        function (data) {
            self.VehicleTypes(data);
            self.IsBusy(false);
        },
        function (error) {
            alert(error.response)
            self.IsBusy(false);
        });
    self.processSearch = function () {
        self.IsBusy(true);
        self.Get("/api/vehicle/search?term=" + self.SearchInput(),
            function (data) {
                self.Vehicles(data);
                self.IsBusy(false);
            },
            function (error) {
                alert(error.response)
                self.IsBusy(false);
            })
    };
    self.processEdit = function (item) {
        self.Id(item.id);
        self.MakeId(item.vehicleMakeId);
        self.BodyTypeId(item.bodyTypeId);
        self.VehicleTypeId(item.vehicleTypeId);
        self.Year(item.year);
        self.Name(item.name); 
        self.IsActive(item.isActive);
    };
    self.processSave = function () {
        if (self.Name() !== "" || self.MakeId() !== "" || self.BodyTypeId() !== "" || self.VehicleTypeId() !== "" || self.Year() !== "") {
            self.Items = ko.observableArray();
            var objectToSave = {
                Id: self.Id(),
                BodyTypeId: self.BodyTypeId(),
                VehicleMakeId: self.MakeId(),
                VehicleTypeId: self.VehicleTypeId(),
                Name: self.Name(),
                Year: self.Year(),
                Image: self.photoUrl(),
                IsActive: self.IsActive()
            };
            self.IsBusy(true);
            self.Post("/api/vehicle/save",
                objectToSave,
                function (data) {
                    alert("Successful");
                    self.Id("");
                    self.BodyTypeId("");
                    self.MakeId("");
                    self.VehicleTypeId("");
                    self.Name("");
                    self.Year("");
                    self.photoUrl(undefined);
                    self.IsActive(false);
                    self.Get("/api/vehicle/get",
                        function (savedData) {
                            self.Items(savedData);
                            self.IsBusy(false);
                        },
                        function (error) {
                            alert(error.response)
                            self.IsBusy(false);
                        });
                },
                function (error) {
                    alert(error);
                    self.IsBusy(false);
                }
            );
        }
        else {
            alert("Please fill in all fields.");
        }
    };

    // Image Upload
    self.fileUpload = function (data, e) {
        var file = e.target.files[0];
        var reader = new FileReader();

        reader.onloadend = function (onloadend_e) {
            var result = reader.result; // base 64 encoded file.
            self.photoUrl(result);
        };

        if (file) {
            reader.readAsDataURL(file);
        }
    };
    self.Get("/api/vehicle/get",
        function (data) {
            self.Vehicles(data);
        },
        function (error) {
            alert(error.response)
        });

}