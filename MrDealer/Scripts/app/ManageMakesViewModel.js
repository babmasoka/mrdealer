﻿function ManageMakesViewModel() {

    self = this;

    BaseViewModel.call(self);

    self.Bookings = ko.observableArray();
    self.Items = ko.observableArray();


    self.SearchInput = ko.observable("").extend({ notify: 'always' });;
    self.Id = ko.observable("").extend({ notify: 'always' });;
    self.Name = ko.observable("").extend({ notify: 'always' });;
    self.IsActive = ko.observable(false).extend({ notify: 'always' });;
    self.Items = ko.observableArray().extend({ notify: 'always' });


    self.IsBusy(true);
    // MAKES FUNCTIONS //
    self.Get("/api/make/get",
        function (data) {
            self.Items(data);
            self.IsBusy(false);
        },
        function (error) {
            alert(error.response)
            self.IsBusy(false);
        });
    self.processSearch = function () {
        self.IsBusy(true);
        self.Get("/api/make/search?term=" + self.SearchInput(),
            function (data) {
                self.Items(data);
                self.IsBusy(false);
            },
            function (error) {
                alert(error.response)
                self.IsBusy(false);
            })
    };
    self.processEdit = function (item) {
        self.Id(item.id);
        self.Name(item.name);
        self.IsActive(item.isActive);
    };
    self.processSave = function () {
        if (self.Name() !== "") {
            var objectToSave = {
                Id: self.Id(),
                Name: self.Name(),
                IsActive: self.IsActive()
            };
            self.IsBusy(true);
            self.Post("/api/make/save",
                objectToSave,
                function (data) {
                    alert("Successful");
                    self.Id('');
                    self.Name('');
                    self.IsActive(false);

                    self.Get("/api/make/get",
                        function (savedData) {
                            self.Items(savedData);
                            self.IsBusy(false);
                        },
                        function (error) {
                            alert(error.response)
                            self.IsBusy(false);
                        });
                },
                function (error) {
                    alert(error);
                    self.IsBusy(false);
                }
            );
        }
        else {
            alert("Please fill in Name field.");
        }
    };
}