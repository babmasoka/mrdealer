﻿function BaseViewModel() {
    var self = this;

    self.Get = function (url, onsuccess, onerror) {

        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            data: {},
            success: onsuccess,
            error: onerror
        });
    };

    self.Post = function (url,object, onsuccess, onerror) {

        $.ajax({
            url: url,
            cache: false,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON(object),
            success: onsuccess,
            error: onerror
        });
    };

    self.IsBusy = function (isBusy) {
        var loadingDiv = $('#loadingDiv');
        var loadingIcon = $('#loadingIcon');

        if (isBusy) {
            loadingDiv.removeClass().addClass('overlay');
            loadingIcon.removeClass().addClass('fa fa-refresh fa-spin');

        } else {
            loadingDiv.removeClass();
            loadingIcon.removeClass();
        }

    };
}