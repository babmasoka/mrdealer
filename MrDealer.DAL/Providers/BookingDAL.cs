﻿using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.DAL.Providers
{
    public class BookingDAL : IBookingDALService
    {
        private readonly MyDealershipEntities _context;

        public BookingDAL(MyDealershipEntities context)
        {
            _context = context;
        }
        public async Task<List<TestDriveBooking>> GetAll()
        {
            return await _context.TestDriveBookings.OrderBy(c => c.ScheduledDate).ToListAsync();
        }

        public async Task<List<TestDriveBooking>> GetAll(Expression<Func<TestDriveBooking, bool>> whereCondition)
        {
            return await _context.TestDriveBookings.Where(whereCondition).OrderBy(c => c.ScheduledDate).ToListAsync();
        }

        public async Task<TestDriveBooking> Save(TestDriveBooking poco)
        {
            var exists = _context.TestDriveBookings.FirstOrDefault(c => c.Id == poco.Id);
            if (exists == null)
            {
                poco.Id = Guid.NewGuid();
                poco.CreatedDate = DateTime.Now;
                _context.TestDriveBookings.Add(poco);
            }
            else
            {
                exists.Time = poco.Time;
                exists.ScheduledDate = poco.ScheduledDate;
                exists.IsActive = poco.IsActive;
                exists.VehicleModelId = poco.VehicleModelId;
                if (!poco.IsActive)
                {
                    exists.DeactivatedDate = DateTime.Now;
                }
                else
                {
                    exists.DeactivatedDate = null;
                }
                _context.Entry(exists).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
            return poco;
        }
    }
}
