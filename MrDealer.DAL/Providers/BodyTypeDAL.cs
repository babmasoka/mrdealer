﻿using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.DAL.Providers
{
    public class BodyTypeDAL : IBodyTypeDALService
    {
        private readonly MyDealershipEntities _context;

        public BodyTypeDAL(MyDealershipEntities context)
        {
            _context = context;
        }
        public async Task<List<BodyType>> GetAll()
        {
            return await _context.BodyTypes.Where(c => c.IsActive).OrderBy(c=>c.Name).ToListAsync();
        }

        public async Task<List<BodyType>> GetAll(Expression<Func<BodyType, bool>> whereCondition)
        {
            return await _context.BodyTypes.Where(whereCondition).OrderBy(c => c.Name).ToListAsync();
        }

        public async Task<BodyType> Save(BodyType poco)
        {
            var exists = _context.BodyTypes.FirstOrDefault(c => c.Id == poco.Id);
            if (exists == null)
            {
                poco.Id = Guid.NewGuid();
                _context.BodyTypes.Add(poco);
            }
            else
            {
                exists.Name = poco.Name;
                exists.IsActive = poco.IsActive;
                if (!poco.IsActive)
                {
                    exists.DeactivatedDate = DateTime.Now;
                }
                else
                {
                    exists.DeactivatedDate = null;
                }
                _context.Entry(exists).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
            return poco;
        }
    }
}
