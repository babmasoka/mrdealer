﻿using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.DAL.Providers
{
    public class VehicleMakeDAL : IVehicleMakeDALService
    {
        private readonly MyDealershipEntities _context;

        public VehicleMakeDAL(MyDealershipEntities context)
        {
            _context = context;
        }
        public async Task<List<VehicleMake>> GetAll()
        {
            return await _context.VehicleMakes.Where(c => c.IsActive).OrderBy(c => c.Name).ToListAsync();
        }

        public async Task<List<VehicleMake>> GetAll(Expression<Func<VehicleMake, bool>> whereCondition)
        {
            return await _context.VehicleMakes.Where(whereCondition).OrderBy(c => c.Name).ToListAsync();
        }

        public async Task<VehicleMake> Save(VehicleMake poco)
        {
            var exists = _context.VehicleMakes.FirstOrDefault(c => c.Id == poco.Id);
            if (exists == null)
            {
                poco.Id = Guid.NewGuid();
                _context.VehicleMakes.Add(poco);
            }
            else
            {
                exists.Name = poco.Name;
                exists.IsActive = poco.IsActive;
                if (!poco.IsActive)
                {
                    exists.DeactivatedDate = DateTime.Now;
                }
                else
                {
                    exists.DeactivatedDate = null;
                }
                _context.Entry(exists).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
            return poco;
        }
    }
}
