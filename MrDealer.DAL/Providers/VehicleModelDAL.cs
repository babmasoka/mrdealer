﻿using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.DAL.Providers
{
    public class VehicleModelDAL : IVehicleModelDALService
    {
        private readonly MyDealershipEntities _context;

        public VehicleModelDAL(MyDealershipEntities context)
        {
            _context = context;
        }

        public async Task<List<VehicleModel>> GetAll()
        {
            return await _context.VehicleModels.Where(c=>c.IsActive).OrderBy(c => c.Name).ToListAsync();
        }

        public async Task<List<VehicleModel>> GetAll(Expression<Func<VehicleModel, bool>> whereCondition)
        {
            return await _context.VehicleModels.Where(whereCondition).OrderBy(c => c.Name).ToListAsync();
        }

        public async Task<VehicleModel> Save(VehicleModel poco)
        {
            var exists = _context.VehicleModels.FirstOrDefault(c => c.Id == poco.Id);
            if (exists == null)
            {
                poco.Id = Guid.NewGuid();
                _context.VehicleModels.Add(poco);
            }
            else
            {
                exists.Name = poco.Name;
                exists.IsActive = poco.IsActive;
                if (!poco.IsActive)
                {
                    exists.DeactivatedDate = DateTime.Now;
                }
                else
                {
                    exists.DeactivatedDate = null;
                }
                exists.Image = poco.Image;
                exists.BodyTypeId = poco.BodyTypeId;
                exists.VehicleMakeId = poco.VehicleMakeId;
                exists.VehicleTypeId = poco.VehicleTypeId;
                exists.Year = poco.Year;
                _context.Entry(exists).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
            return poco;
        }

    }
}
