﻿using MrDealer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.DAL.Services
{
    public interface IVehicleModelDALService
    {
        Task<List<VehicleModel>> GetAll();
        Task<List<VehicleModel>> GetAll(Expression<Func<VehicleModel, bool>> whereCondition);
        Task<VehicleModel> Save(VehicleModel poco);
    }
}
