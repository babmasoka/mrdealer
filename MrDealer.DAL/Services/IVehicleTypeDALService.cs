﻿using MrDealer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.DAL.Services
{
    public interface IVehicleTypeDALService
    {
        Task<List<VehicleType>> GetAll();
        Task<List<VehicleType>> GetAll(Expression<Func<VehicleType, bool>> whereCondition);
        Task<VehicleType> Save(VehicleType poco);
    }
}
