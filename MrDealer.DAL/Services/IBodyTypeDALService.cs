﻿using MrDealer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.DAL.Services
{
    public interface IBodyTypeDALService
    {
        Task<List<BodyType>> GetAll();
        Task<List<BodyType>> GetAll(Expression<Func<BodyType, bool>> whereCondition);
        Task<BodyType> Save(BodyType poco);
    }
}
