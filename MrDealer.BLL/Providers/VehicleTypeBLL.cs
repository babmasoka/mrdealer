﻿using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.BLL.Providers
{
    public class VehicleTypeBLL : IVehicleTypeBLLService
    {
        private readonly IVehicleTypeDALService _dALService;

        public VehicleTypeBLL(IVehicleTypeDALService dALService)
        {
            _dALService = dALService;
        }
        public async Task<List<VehicleType>> GetAll()
        {
            return await _dALService.GetAll();
        }

        public async Task<List<VehicleType>> GetAll(Expression<Func<VehicleType, bool>> whereCondition)
        {
            return await _dALService.GetAll(whereCondition);
        }

        public async Task<VehicleType> Save(VehicleType poco)
        {
            return await _dALService.Save(poco);
        }
    }
}
