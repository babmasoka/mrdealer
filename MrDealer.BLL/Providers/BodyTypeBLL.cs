﻿using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.BLL.Providers
{
    public class BodyTypeBLL : IBodyTypeBLLService
    {
        private readonly IBodyTypeDALService _dALService;

        public BodyTypeBLL(IBodyTypeDALService dALService)
        {
            _dALService = dALService;
        }
        public async Task<List<BodyType>> GetAll()
        {
            return await _dALService.GetAll();
        }

        public async Task<List<BodyType>> GetAll(Expression<Func<BodyType, bool>> whereCondition)
        {
            return await _dALService.GetAll(whereCondition); 
        }

        public async Task<BodyType> Save(BodyType poco)
        {
            return await _dALService.Save(poco);
        }
    }
}
