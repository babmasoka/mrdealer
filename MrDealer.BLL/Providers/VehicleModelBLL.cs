﻿using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.BLL.Providers
{
    public class VehicleModelBLL : IVehicleModelBLLService
    {
        private readonly IVehicleModelDALService _dALService;

        public VehicleModelBLL(IVehicleModelDALService dALService)
        {
            this._dALService = dALService;
        }

        public async Task<VehicleModel> SaveModel(VehicleModel poco)
        {
            return await _dALService.Save(poco);
        }

        public async Task<List<VehicleModel>> GetAll()
        {
            return await _dALService.GetAll();
        }

        public async Task<List<VehicleModel>> GetAll(Expression<Func<VehicleModel, bool>> whereCondition)
        {
            return await _dALService.GetAll(whereCondition);
        }
    }
}
