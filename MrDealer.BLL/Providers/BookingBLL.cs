﻿using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.BLL.Providers
{
    public class BookingBLL : IBookingBLLService
    {
        private readonly IBookingDALService _dALService;

        public BookingBLL(IBookingDALService dALService)
        {
            _dALService = dALService;
        }
        public async Task<List<TestDriveBooking>> GetAll()
        {
            return await _dALService.GetAll();
        }

        public async Task<List<TestDriveBooking>> GetAll(Expression<Func<TestDriveBooking, bool>> whereCondition)
        {
            return await _dALService.GetAll(whereCondition);
        }

        public async Task<TestDriveBooking> Save(TestDriveBooking poco)
        {
            return await _dALService.Save(poco);
        }
    }
}
