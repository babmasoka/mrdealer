﻿using MrDealer.BLL.Services;
using MrDealer.Core;
using MrDealer.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.BLL.Providers
{
    public class VehicleMakeBLL : IVehicleMakeBLLService
    {
        private readonly IVehicleMakeDALService _dALService;

        public VehicleMakeBLL(IVehicleMakeDALService dALService)
        {
            _dALService = dALService;
        }
        public async Task<List<VehicleMake>> GetAll()
        {
            return await _dALService.GetAll();
        }

        public async Task<List<VehicleMake>> GetAll(Expression<Func<VehicleMake, bool>> whereCondition)
        {
            return await _dALService.GetAll(whereCondition); 
        }

        public async Task<VehicleMake> Save(VehicleMake poco)
        {
            return await _dALService.Save(poco);
        }
    }
}
