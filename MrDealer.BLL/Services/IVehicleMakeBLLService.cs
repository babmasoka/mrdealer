﻿using MrDealer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.BLL.Services
{
    public interface IVehicleMakeBLLService
    {
        Task<List<VehicleMake>> GetAll();
        Task<List<VehicleMake>> GetAll(Expression<Func<VehicleMake, bool>> whereCondition);
        Task<VehicleMake> Save(VehicleMake poco);
    }
}
