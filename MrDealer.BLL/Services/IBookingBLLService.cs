﻿using MrDealer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MrDealer.BLL.Services
{
    public interface IBookingBLLService
    {
        Task<List<TestDriveBooking>> GetAll();
        Task<List<TestDriveBooking>> GetAll(Expression<Func<TestDriveBooking, bool>> whereCondition);
        Task<TestDriveBooking> Save(TestDriveBooking poco);
    }
}
